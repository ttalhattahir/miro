package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    //////////***************PageObject Variables********************************
    By emailField = By.id("email");
    By passwordField = By.id("password");
    By signInButton = By.className("signup__submit");
    By errorMessage= By.className("signup__error-item");
    By forgotPassword = By.xpath("//*[@href='/recover/']");

    //***********************Methods********************************************

    @Step("Enter email in the email field")
    public void enterTextInEmailField(String email) {
        explicitWait(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(email);
    }

    @Step("Enter password in the password field")
    public void enterTextInPasswordField(String password) {
        driver.findElement(passwordField).sendKeys(password);
    }

    @Step("Click on Sign In button")
    public void clickSignInButton() {
        driver.findElement(signInButton).submit();
    }

    @Step("Click on forgot password text link")
    public void clickForgotPassword() {
        driver.findElement(forgotPassword).click();
    }

    public String getTextOfEmailErrorMessage() {
        return driver.findElement(errorMessage).getText();
    }

    public String getTextOfPasswordErrorMessage() {
        return driver.findElement(errorMessage).getText();
    }

    public String getTextOfEmailPasswordErrorMessage() {
        explicitWait(errorMessage);
        return driver.findElement(errorMessage).getText();
    }

    public String getTextForPasswordNotSet() {
        explicitWait(errorMessage);
         return driver.findElement(errorMessage).getText();
    }

    public LoginPage(WebDriver driver) {this.driver = driver;}
}

