package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {
    //////////***************PageObject Variables********************************
    By profileIcon = By.className("user-profile__button");
    By userProfileEmail = By.className("user-profile__email");

    //***********************Methods********************************************

    public HomePage (WebDriver driver) {this.driver = driver;}

    public void clickProfileIcon() {
        explicitWait(profileIcon);
        driver.findElement(profileIcon).click();
    }

    public String getUserEmail() {
        explicitWait(userProfileEmail);
        return driver.findElement(userProfileEmail).getText();
    }

}
