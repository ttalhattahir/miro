package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ForgotPasswordPage extends BasePage{

    //////////***************PageObject Variables********************************
    By continueButton = By.className("signup__submit-wrap");
    By successPageText = By.className("signup__container");
    By backToSignInPage = By.cssSelector("button[class='signup__submit signup__submit--shrink']");
    By backToSignInTextLink = By.xpath("//*[@href='/login/']");

    //***********************Methods********************************************
    @Step("Click on continue button")
    public void clickContinueButton() {
        driver.findElement(continueButton).click();
    }

    @Step("Click on back to sign in text link")
    public void clickBackToSignInTextLink() {
        driver.findElement(backToSignInTextLink).click();
    }

    @Step("Click Back to sign in button")
    public void clickBackToSignInButton() {
        driver.findElement(backToSignInPage).click();
    }

    public String getSuccessEmailSentPageText() {
        explicitWait(successPageText);
        return driver.findElement(successPageText).getText();
    }

    public ForgotPasswordPage(WebDriver driver) {
        this.driver = driver;
    }
}
