package tests;

import org.testng.annotations.*;

import io.qameta.allure.Description;
import org.testng.Assert;
import pages.HomePage;
import pages.LoginPage;

public class LoginTest extends BaseTest {
    LoginPage loginPageObj;
    HomePage homePageObj;

    @DataProvider(name="invalidLoginTestData")
    public static Object[][] loginTestData()
    {
        Object[][] data=new Object[3][2];
        data[0][0]="invalidemail3";
        data[0][1]="Test1234";
        data[1][0]="ttalhattahir+1@gmail.com";
        data[1][1]="randomwrongpassword1";
        data[2][0]="wrong email1";
        data[2][1]="wrong password1";
        return data;
    }

    @BeforeClass
    public void initializeObjects() {
        getDriver();
        readFile();
        loginPageObj = new LoginPage(driver);
        homePageObj = new HomePage(driver);
        Assert.assertEquals(driver.getCurrentUrl(),"https://miro.com/login/");
    }

    @Test
    @Description("Verify login success with valid email and password ")
    public void loginToWebsiteWithValidCredentials() {
        loginPageObj.enterTextInEmailField(prop.getProperty("validEmailId"));
        loginPageObj.enterTextInPasswordField(prop.getProperty("validPassword"));
        loginPageObj.clickSignInButton();
        homePageObj.clickProfileIcon();
        Assert.assertEquals(homePageObj.getUserEmail(),prop.getProperty("validEmailId"));
    }

    @Test
    @Description("Verify error message for invalid/empty email")
    public void loginToWebsiteWithEmptyEmail() {
        loginPageObj.enterTextInPasswordField(prop.getProperty("validPassword"));
        loginPageObj.clickSignInButton();
        Assert.assertEquals(loginPageObj.getTextOfEmailErrorMessage(),"Please enter your email address.");
    }

    @Test
    @Description("Verify error message for invalid/empty password")
    public void loginToWebsiteWithEmptyPassword() {
        loginPageObj.enterTextInEmailField(prop.getProperty("validEmailId"));
        loginPageObj.clickSignInButton();
        Assert.assertEquals(loginPageObj.getTextOfPasswordErrorMessage(),"Please enter your password.");
    }

    @Test(dataProvider = "invalidLoginTestData")
    @Description("Verify login with invalid credentials will prompt error message")
    public void loginToWebsiteWithInvalidCredentials(String email, String password) {
        loginPageObj.enterTextInEmailField(email);
        loginPageObj.enterTextInPasswordField(password);
        loginPageObj.clickSignInButton();
        Assert.assertEquals(loginPageObj.getTextOfEmailPasswordErrorMessage(),
                "The email or password you entered is incorrect.\n" +
                        "Please try again.");
    }

   // @Test
    public void passwordNotSetCheck() {
        loginPageObj.enterTextInEmailField("ttalhattahir@gmail.com");
        loginPageObj.enterTextInPasswordField("123");
        loginPageObj.clickSignInButton();
        Assert.assertEquals(loginPageObj.getTextForPasswordNotSet(),
                "You have not set a password for this account. Set password.");
    }

    @AfterClass
    public void tearDown() {
        loginPageObj = null;
        homePageObj = null;
        quitBrowser();
    }
}
