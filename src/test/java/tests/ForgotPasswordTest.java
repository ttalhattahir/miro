package tests;

import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ForgotPasswordPage;
import pages.LoginPage;

public class ForgotPasswordTest extends BaseTest{

    LoginPage loginPageObj;
    ForgotPasswordPage forgotPaswrdPageObj;

    @BeforeClass
    public void initializeObjects() {
        getDriver();
        readFile();
        loginPageObj = new LoginPage(driver);
        forgotPaswrdPageObj = new ForgotPasswordPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(),"https://miro.com/login/");
        loginPageObj.clickForgotPassword();
    }

    @Test
    @Description("Verify forgot password flow for valid email")
    public void forgotPasswordFlowSuccessForValidEmail() {
        loginPageObj.enterTextInEmailField(prop.getProperty("validEmailId"));
        forgotPaswrdPageObj.clickContinueButton();
        Assert.assertTrue(forgotPaswrdPageObj.getSuccessEmailSentPageText().contains("Thank you!"));
        forgotPaswrdPageObj.clickBackToSignInButton();
        Assert.assertEquals(driver.getTitle(),"Miro | Online Whiteboard for Visual Collaboration");
    }

    @Test
    @Description("Verify click on sign navigates user to login page")
    public void clickSignInNavigatesToLoginPage() {
        forgotPaswrdPageObj.clickBackToSignInTextLink();
        Assert.assertEquals(driver.getTitle(),"Miro | Online Whiteboard for Visual Collaboration");
        loginPageObj.clickForgotPassword();
    }

    @Test
    @Description("Verify invalid email gives no success in forgot password flow")
    public void invalidEmailFlowForgotPassword() {
        loginPageObj.clickForgotPassword();
        loginPageObj.enterTextInEmailField(prop.getProperty("invalidEmail"));
        forgotPaswrdPageObj.clickContinueButton();
        Assert.assertEquals(driver.getCurrentUrl(),"https://miro.com/recover/");
    }

    @AfterClass
    public void tearDown() {
        loginPageObj = null;
        forgotPaswrdPageObj = null;
        quitBrowser();
    }

}
